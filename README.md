# Fast Quarantine

This project publishes simple `.txt` files that include one test entity identifier per line.
These files are then published as Pages, e.g.
https://gitlab-org.gitlab.io/quality/engineering-productivity/fast-quarantine/rspec/fast_quarantine-gitlab.txt.

The goal is to allow to fast-quarantine tests without the need to open & merge a merge request in [the main GitLab project](https://gitlab.com/gitlab-org/gitlab).

This is mostly intended for un-blocking deployments blocked due to flaky/broken tests.

To resolve a failed CI job using this method, restarting the job by itself will not automatically pick up the fast-quarantine update. Instead, we need to run a new pipeline, or rerun the `retrieve-test-metadata` job (`download-fast-quarantine-report` in e2e pipelines) to re-download the quarantine list, before retrying the failed job.

This implements the idea proposed at https://gitlab.com/gitlab-org/quality/engineering-productivity/team/-/issues/204.

### Fast Quarantine for Dedicated

There is a second fast quarantine file for Dedicated, [fast_quarantine-dedicated.txt](./rspec/fast_quarantine-dedicated.txt). This was implemented because Dedicated runs a version back from GitLab (and does not pickup minor releases). So it will often be out of sync with what tests need to be fast quarantined from the current release. In order to switch to using this quarantine file, set the environment variable `RSPEC_FAST_QUARANTINE_FILE` to `fast_quarantine-dedicated.txt` otherwise you will use the default `fast_quarantine-gitlab.txt` file.

## Supported test frameworks

Fast-quarantine is supported for:

- [the main GitLab project](https://gitlab.com/gitlab-org/gitlab)'s RSpec test suite
- [the end-to-end test suite](https://gitlab.com/gitlab-org/gitlab/-/tree/master/qa/qa/specs)

## Fast-quarantine a test

- [ ] Create a merge request using [the default template](.gitlab/merge_request_templates/Default.md)
- [ ] Follow the checklist
